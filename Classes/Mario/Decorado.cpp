//
//  Decorado.cpp
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 16/1/15.
//
//

#include "Decorado.h"

bool Decorado::init(){
    GameEntity::init();
    return true;
}



Node* Decorado::getNode(){
    if(m_node==NULL) {
        
        Size visibleSize = Director::getInstance()->getVisibleSize();
        
        //Decorados
        auto cielo = LayerGradient::create(Color4B(100,100,255,255),Color4B(0,0,100,255));
        cielo->setContentSize(Size(3392+500, visibleSize.height/2+200));
        cielo->setPosition(-500,visibleSize.height/2);
        
        auto fondo = LayerGradient::create(Color4B(70,0,0,255),Color4B(255,100,100,255));
        fondo->setContentSize(Size(3392+500, visibleSize.height/2));
        fondo->setPosition(-500,0);
        
        
        m_node= Node::create();
        m_node->addChild(cielo,0);
        m_node->addChild(fondo,0);
        m_node->setLocalZOrder(-1);
    }
    
    return m_node;
}


