//
//  MarioSc.cpp
//  Test01
//
//  Created by Fidel Aznar on 13/1/15.
//
//

#include "Game.h"
#include "VirtualPad.h"
#include "VirtualStick.h"
#include "VirtualStickAuto.h"

Mundo* Mundo::m_instance = NULL;

bool Game::init(){
    
    SimpleGame::init();
    
    m_decorado = Decorado::create();
    m_decorado->retain();
    addGameEntity(m_decorado);
    
    m_mundo = Mundo::getInstance();
    addGameEntity(m_mundo);
    
    m_gui = GUI::create();
    m_gui->retain();
    addGameEntity(m_gui);
    
    m_player = PhysicsPlayer::create();
    m_player->retain();
    addGameEntity(m_player);
    
    // Controles virtuales
    m_input = VirtualStick::create();
    m_input->retain();
    addGameEntity(m_input);
    
    this->preloadResources();
    this->start();
    
    return true;
}

Game::~Game(){
    m_player->release();
    m_decorado->release();
    m_gui->release();
    m_input->release();
}

void Game::preloadResources(){
    preloadEachGameEntity();
}

void Game::start(){
    
    Node *scrollable = Node::create();
    
    addEachGameEntityNodeTo(scrollable);
    scrollable->removeChild(m_gui->getNode());
    scrollable->removeChild(m_input->getNode());
    
    addChild(scrollable);
    addChild(m_gui->getNode());
    addChild(m_input->getNode());

    m_input->onButtonPressed = CC_CALLBACK_1(Game::onButtonPressed, this);
    m_input->onButtonReleased = CC_CALLBACK_1(Game::onButtonReleased, this);
    m_input->addKeyboardListeners(this);
    
    m_gui->setVidas(m_player->getNumVidas());
    
    //La camara que siga al personaje (ver http://www.learn-cocos2d.com/2012/12/ways-scrolling-cocos2d-explained/)
    //No se puede agregar a la escena directamente, hay que crear un nodo para el scroll o da problemas con la cámara
    scrollable->runAction(Follow::create(m_player->getNode()));
    
}

void Game::updateEachFrame(float delta){

    updateEachGameEntityWithDelta(delta);

    // (d) Descomentar. Control de usuario con eje horizontal de controles virtuales
    m_player->move(m_input->getAxis(Axis::AXIS_HORIZONTAL), delta);
    
    testNPCCollisionsWithPlayer();
}

void Game::testNPCCollisionsWithPlayer(){
    
    //Compruebo colisiones entre Player y el primer NPC...
    auto bplayer = m_player->getNode()->getBoundingBox();
    auto bnpc = m_mundo->getNPCs().front()->getNode()->getBoundingBox();
    
    if (bplayer.intersectsRect(bnpc)) {
        m_player->die();
        m_gui->setVidas(m_player->getNumVidas());
    }
}

void Game::onButtonPressed(Button button) {
    if(button == Button::BUTTON_ACTION)
    {
        m_player->jump();
    }
}

void Game::onButtonReleased(Button button) {
}

