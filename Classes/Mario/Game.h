//
//  MarioSc.h
//  Test01
//
//  Created by Fidel Aznar on 13/1/15.
//
//

#pragma once

#include "../Engine2D/SimpleGame.h"
#include "PhysicsPlayer.h"
#include "VirtualControls.h"
#include "Npc.h"
#include "Decorado.h"
#include "GUI.h"
#include "Mundo.h"

class Game: public SimpleGame{
    
public:
    
    bool init();
        ~Game();
    
    void preloadResources();
    void start();
    
    void updateEachFrame(float delta);
    
    void testNPCCollisionsWithPlayer();
    
    void onButtonPressed(Button button);
    void onButtonReleased(Button button);
    
    // implement the "static create()" method manually
    CREATE_FUNC(Game);
    
private:
    PhysicsPlayer *m_player;
    
    Decorado *m_decorado;
    GUI *m_gui;
    Mundo *m_mundo;
    VirtualControls *m_input;
    
};


