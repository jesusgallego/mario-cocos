//
//  Npc.h
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 16/1/15.
//
//

#pragma once

#define ACTION_GRAVITY  005

#include "../Engine2D/GameEntity.h"

class Mundo;

class NPC: public GameEntity{
    
public:
    
    bool init();
    
    void preloadResources();
    Node* getNode();
    void update(float delta);
    
    void move(bool right);
    void activeGravity();
    void stopGravity();
    
    void testWorldCollisions(Mundo*);
    void testWorldGravity(Mundo*);
    
    Point initPos = Point(0.0,0.0);
    
    
    CREATE_FUNC(NPC);
    
    
private:
    
    Sprite *m_sprite;
    Animation *m_anim;
    
};
